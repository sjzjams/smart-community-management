import Vue from 'vue'
import App from './App'
import store from './store'
import httpServer from './util/httpServer'

Vue.config.productionTip = false

Vue.prototype.$store = store
Vue.prototype.$backgroundAudioData = {
	playing: false,
	playTime: 0,
	formatedPlayTime: '00:00:00'
}
Vue.prototype.$adpid = ""
Vue.prototype.$myRequest = httpServer
App.mpType = 'app'

// 引入全局uView
import uView from 'uview-ui'
Vue.use(uView);

const app = new Vue({
	store,
	...App
})
app.$mount()
