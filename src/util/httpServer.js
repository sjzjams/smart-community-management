/**
 *公共请求方法
 *默认GET请求
 *
 */
 const httpTokenRequest = (url, data={},type='GET') => {
    // let baseUrl='http://10.10.13.200:9800'
    let baseUrl=getApp().globalData.baseUrl
    let token = "";
    if(uni.getStorageSync('token')){
        token =uni.getStorageSync('token')
    }
    let httpDefaultOpts = {
        url: baseUrl + url,
        data: data,
        method:type,
        header:type == 'GET' ? {
            'X-Requested-With': 'XMLHttpRequest',
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        } : {
            'X-Requested-With': 'XMLHttpRequest',
            // 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            'Content-Type': 'application/json; charset=UTF-8'
        },
        dataType: 'json',
    }
    if(token){
        httpDefaultOpts.header.token=token
    }
    // uni.showLoading({
    //     title: '数据加载中...',
    // });
    return new Promise(function(resolve, reject) {
        uni.request({
            ...httpDefaultOpts,
            success:function (res) {
                // uni.hideLoading()
                if(isTokenError(res)){
                    uni.showModal({
                        title: '提示',
                        content: 'Token无效,请重新登陆',
                        showCancel: false,
                        confirmText: '重新登录',
                        success: function(res) {
                            if(res.confirm){
                                uni.reLaunch({
                                    url: '/pages/index/index',
                                });
                            }
                        }
                    })
                }else if(!isHttpSuccess(res)){
                    http_error(res);
                }else if(!isCodeSuccess(res)){
                    code_error(res);
                }else{
                    resolve(res.data,res);
                }
            },
            fail: (res) => {
                http_error(res);
                uni.hideLoading()
            },
        })
    })
};
function isHttpSuccess(res) {
    let status = res.statusCode;
    let isSuccess = status >= 200 && status < 300 || status === 304;
    return isSuccess;
}
function isCodeSuccess(res) {
    let code = res.data.status;
    if (code == 200) {
        return true;
    }
    return false;
}
function isTokenError(res) {
    let code = res.data.status;
    if (code == 304) {
        return true;
    }
    return false;
}
function http_error(res) {
    let message = "网络请求出现错误,请稍后重试。"
    if(res && res.data && res.data.msg){
        message = res.data.msg;
    }
    uni.showModal({
        title: '网络请求错误',
        content: message,
        showCancel: false,
        confirmText: '返回首页',
        success(res) {
            if (res.confirm) {
                uni.reLaunch({
                    url: '../index/index',
                });
            }
        }
    })
}
function code_error(res) {
    let message = "业务处理出现错误,请稍后重试。"
    if (res && res.data && res.data.msg) {
        message = res.data.msg;
    }
    uni.showToast({
        title: message,
        icon: 'none'
    });
}
export default httpTokenRequest

