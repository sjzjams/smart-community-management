/**
 *公用函数
 */
 const utils={
    /**
     *手机号校验
     */
    testPhone(mobile) {
        if (mobile == "") {
            uni.showToast({
                title: "手机号码不能为空",
                icon: "none",
                duration: 2000
            });

            return false;
        } else if (
            !/^1[2|3|4|5|6|7|8|9][0-9]\d{8,11}$/.test(mobile) &&
            mobile != ""
        ) {
            uni.showToast({
                title: "手机号码格式不正确",
                icon: "none",
                duration: 2000
            });
            return false;
        } else {
            return true;
        }
    },
    /**
     *获取设备高度
     */
    getHeight(type) {
        let windowHeight = 0;
        uni.getSystemInfo({
            success: res => {
                windowHeight = res.windowHeight;
                if (type != 1) {
                    windowHeight = res.windowHeight - 44;
                }
            }
        });
        return windowHeight;
    },
    /**
     *判断是否为空值
     */
    isEmpty (str) {
        if (typeof str == "undefined" || str == null || str == "") {
            return true;
        }
        return false;
    },
    /**
     *判断一个对象数组中的某一个key值是否为XX
     * 参数说明
     * key:对象的key name为你想判断的内容 arr对象数组
     */
    checkArrKeyIndex(key,name,arr){
        let isHas=false
        if(Array.isArray(arr) && arr.length>0){
            arr.map(item=>{
                if(item[key]===name){
                    isHas=true
                }
            })
        }
        return isHas
    },


}
export default utils
